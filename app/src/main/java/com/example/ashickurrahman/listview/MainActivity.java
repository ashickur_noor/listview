package com.example.ashickurrahman.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.*;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayAdapter<String>adapter;
    ArrayList<String> country=new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        country.add("BD");
        country.add("KSA");
        country.add("IND");
        country.add("NZ");
        listView=findViewById(R.id.listview);
        adapter=new ArrayAdapter<String>(this,R.layout.layout2,country);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, country.get(position), Toast.LENGTH_SHORT).show();
                country.remove(position);
                adapter.notifyDataSetChanged();//


            }
        });


    }
}
